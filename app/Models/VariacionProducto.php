<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VariacionProducto extends Model
{
    use HasFactory;

    public $table = 'var_producto';

    protected $fillable = [
        'prod_base_id',
        'referencia',
        'precio'
    ];

    public $timestamps = false;
}
