<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductoBase;
use App\Models\VariacionProducto;
use Illuminate\Support\Str;

class ControladorProductos extends Controller
{
    /* 
    |_______________________________
    | funciones para crear productos 
    |_______________________________
    |
    */

    //funcion para crear los productos principales o base
    public function CrearProducto(Request $request) {
        $request->validate([
            'nombre_producto' => 'required|string|max:150',
            'referencia' => 'required|integer',
            'precio' => 'required|numeric'
        ]);
        try {
            ProductoBase::create([
                'nombre_prod' => request('nombre_producto'),
                'referencia' => request('referencia'),
                'precio' => request('precio')
            ]);                
            return response('producto creado satisfactoriamente.', 200);
        } catch (\Throwable $th) {
            if(Str::contains($th, 'Duplicate entry')) {
                return response('ya existe un producto con esa referencia');
            } else {
                return response('ocurrio algun error al crear el producto.');
            }
        }            
    }

    //function para crear las variantes de producto o subproductos
    public function CrearVariacionProducto (Request $request) {
        $request->validate([
            'prod_base_id' => 'required|integer',
            'referencia' => 'required|integer',
            'precio' => 'required|numeric'
        ]);
        //buscamos el id del producto base            
        $baseId = ProductoBase::where('id', request('prod_base_id'))->select('id')->first();

        //verificamos que el producto base exista antes de hacer el query
        if($baseId == null) {
            return response('no existe ese producto base.');
        } else {
            VariacionProducto::create([
                'prod_base_id' => $baseId->id,
                'nombre_prod' => request('nombre_producto'),
                'referencia' => request('referencia'),
                'precio' => request('precio')
            ]);

            return response('variacion producto creado satisfactoriamente.', 200);
        }
        
    }

    /* 
    |___________________________
    | funciones para seleccionar
    | productos 
    |___________________________
    |
    */

    //lista de productos
    public function ListaProductos () {
        $productos = ProductoBase::get();

        return $productos;
    }

    //lista de variantes de producto
    public function VariantesProducto ($base_id) {
        $variantes_producto = VariacionProducto::where('prod_base_id', $base_id)
            ->join('producto_base', 'var_producto.prod_base_id', '=', 'producto_base.id')
            ->select('producto_base.nombre_prod', 'var_producto.referencia', 'var_producto.precio')
            ->get();

        return $variantes_producto;
    }

    /* 
    |___________________________
    | funciones para actualizar
    | productos 
    |___________________________
    |
    */

    //actualizar producto base
    public function ActualizarProducto() {
        if(request('id') && is_numeric(request('precio'))) {
            try {
                $producto = ProductoBase::where('id', '=', request('id'))->first();
                if($producto == null) {
                    return response('no existe ese producto');
                } else {
                    $producto->update([
                        'precio' => request('precio')
                    ]);
                    return response('precio del producto actualizado con exito');
                } 
            } catch (\Throwable $th) {
                return response('ha ocurrido algun error');
            }            
        }
    }

    //actualizar variacion producto
    public function ActualizarVariacionProducto() {
        if(request('id') && is_numeric(request('precio'))) {
            try {
                $producto = VariacionProducto::where('id', '=', request('id'))->first();
                if($producto == null) {
                    return response('no existe esa variacion de producto');
                } else {
                    $producto->update([
                        'precio' => request('precio')
                    ]);
                    return response('precio de la variante del producto actualizado con exito');
                }
            } catch (\Throwable $th) {
                return response('ha ocurrido algun error');
            }            
        }
    }

    /* 
    |___________________________
    | funciones para eliminar
    | productos 
    |___________________________
    |
    */

    //funcion para eliminar producto
    public function EliminarProducto() {
        if (is_numeric(request('id'))) {
            $producto = ProductoBase::where('id', '=', request('id'))->first();
            if($producto == null) {
                return response('no existe el producto solicitado');
            } else {
                $producto->delete(); //al eliminar el producto, las variaciones del mismo se eliminan
                return response('producto y variaciones eliminadas con exito');
            }
        } else {
            return response('solicitud invalida');
        }
    }

    public function EliminarVariacionProducto() {
        if (is_numeric(request('id'))) {
            $producto = VariacionProducto::where('id', '=', request('id'))->first();
            if($producto == null) {
                return response('no existe el producto solicitado');
            } else {
                $producto->delete();
                return response('variacion de producto eliminada con exito');
            }
        } else {
            return response('solicitud invalida');
        }
    }
}
