<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('var_producto', function(Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('prod_base_id');
            $table->foreign('prod_base_id')
                ->references('id')
                ->on('producto_base')->onDelete('cascade');
            $table->string('referencia');
            $table->float('precio');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
