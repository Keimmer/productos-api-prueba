Proyecto prueba - API Productos

Elaborado en laravel 9

Para inicializar el proyecto se debe ejecutar el comando 'composer install', luego, crear la base de datos con el nombre 'productos' y luego se debe ejecutar el comando: php artisan migrate

Con la base de datos preparada se ejecuta el servidor con el comando php artisan serve

Las rutas disponibles son:

api/crear-producto-base; > POST, para crear un producto se requieren 'nombre_producto', 'referencia', 'precio'
api/crear-variante-producto; > POST, para crear una variante de producto se requieren 'prod_base_id', 'nombre_producto', 'referencia', 'precio'

api/productos; > GET, esta ruta nos muestra un objeto con todos los productos disponibles
api/variantes/producto/{base_id}; > GET, esta ruta nos muestra un objeto con todas las variantes del producto solicitado

api/actualizar-producto; POST > en esta ruta se nos permite actualizar el precio de cualquier producto, se requieren: 'id', 'precio'
api/actualizar-variacion-producto; POST > en esta ruta se nos permite actualizar el precio de cualquier variacion de producto, se requieren: 'id', 'precio'


api/eliminar-producto; POST > en esta ruta se nos permite eliminar cualquier producto, al eliminarse el producto se eliminan sus variantes, se requiere: 'id'
api/eliminar-variacion-producto; POST > en esta ruta se nos permite eliminar cualquier variante de producto, se requiere: 'id'

