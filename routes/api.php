<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ControladorProductos;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

/* 
|---------------------------
|   Rutas de los productos
|---------------------------
|
|
*/

//Rutas para crear
Route::post('crear-producto-base', [ControladorProductos::class, 'CrearProducto']);
Route::post('crear-variante-producto', [ControladorProductos::class, 'CrearVariacionProducto']);

//Rutas para listar productos
Route::get('productos', [ControladorProductos::class, 'ListaProductos']);
Route::get('variantes/producto/{base_id}', [ControladorProductos::class, 'VariantesProducto']);

//Rutas para actualizar productos
Route::post('actualizar-producto', [ControladorProductos::class, 'ActualizarProducto']);
Route::post('actualizar-variacion-producto', [ControladorProductos::class, 'ActualizarVariacionProducto']);

//Rutas para eliminar productos
Route::post('eliminar-producto', [ControladorProductos::class, 'EliminarProducto']);
Route::post('eliminar-variacion-producto', [ControladorProductos::class, 'EliminarVariacionProducto']);